package main

import (
	"fmt"
	"math"
	"strconv"
)

// binarySearch
//
//	@Description: 二分查找
//	@param arr
//	@param target
//	@return int
func binarySearch(arr []int, target int) int {
	left := 0
	right := len(arr) - 1
	for left <= right {
		mid := left + (right-left)/2
		if arr[mid] == target {
			return mid
		}
		if arr[mid] < target {
			left = mid + 1
		} else {
			right = mid - 1
		}
	}
	return -1
}
func quicksort(arr []int, start int, end int) {
	if start >= end {
		return
	}
	key := arr[start]
	i, j := start, end
	for i < j {
		for i < j && arr[j] >= key {
			j--
		}
		arr[i] = arr[j]
		for i < j && arr[i] <= key {
			i++
		}
		arr[j] = arr[i]
	}
	arr[i] = key
	quicksort(arr, start, i-1)
	quicksort(arr, i+1, end)
}

// ------------ 删除连表倒数第N个节点------------
type Node struct {
	Value int
	Next  *Node
}

func printLinkedList(head *Node) {
	current := head
	for current != nil {
		fmt.Print(current.Value, " ")
		current = current.Next
	}
	fmt.Println()
}
func removeNode(head *Node, n int) *Node {
	tmp := &Node{Value: 0, Next: head}
	first, second := tmp, tmp
	for i := 0; i <= n; i++ {
		first = first.Next
	}
	for first != nil {
		first = first.Next
		second = second.Next
	}
	second.Next = second.Next.Next
	return tmp.Next
}
func createLinkedList(arr []int) *Node {
	head := &Node{Value: arr[0]}
	current := head
	for _, val := range arr[1:] {
		current.Next = &Node{Value: val}
		current = current.Next
	}
	return head
}

// ------------ 删除连表倒数第N个节点------------

func main() {
	arr := []int{2, 1, 4, 5, 3, 6, 0, 9, 7, 8}
	quicksort(arr, 0, len(arr)-1)
	fmt.Println(arr)
	// fmt.Println(arr)
	// // fmt.Println(binarySearch([]int{1, 4, 3, 2, 7, 5, 9, 6, 8}, 5))
	// // wordCount("hello bill")
	// // twoSum([]int{1, 2, 3, 2, 5}, 4)
	// // isPalindrome(1221)
	// // fmt.Println(longestCommonPrefix([]string{"cir", "car"}))
	// // fmt.Println(isValidCom("){"))
	// // l1 := &ListNode{Val: 1, Next: &ListNode{Val: 2, Next: &ListNode{Val: 4}}}
	// // l2 := &ListNode{Val: 1, Next: &ListNode{Val: 3, Next: &ListNode{Val: 4}}}
	// // l := mergeTwoLists(l1, l2)
	// // for l != nil {
	// // 	fmt.Println(l.Val)
	// // 	l = l.Next
	// // }
	// // removeDuplicates([]int{0, 0, 1, 1, 1, 2, 2, 3, 3, 4})
	// // arr := []int{0, 1, 2, 2, 3, 0, 4, 2}
	// // fmt.Println(arr)
	// // removeElement(arr, 2)
	// // fmt.Println(arr)
	// // fmt.Println(strStr("adbutsad", "sad"))
	// // fmt.Println(searchInsert([]int{1, 3, 5, 6}, 0))
	// // fmt.Println(lengthOfLastWord("a"))
	// // fmt.Println(plusOne([]int{8, 8, 8}))
	// // fmt.Println(addBinary("11", "1"))
	// // fmt.Println(climbStairs(1))
	// // fmt.Println(climbStairs(2))
	// // fmt.Println(climbStairs(3))
	// // fmt.Println(climbStairs(4))
	// // fmt.Println(climbStairs(5))
	// // fmt.Println(reverse(123))
	// // fmt.Println(reverse(-123))
	// // fmt.Println(reverse(120))
	// // fmt.Println(reverse(0))
	// // fmt.Println(reverse(1534236469))
	// // fmt.Println(reverseStr("hellob", 2))
	// // arr1 := []int{1, 2, 3, 0, 0, 0}
	// // arr2 := []int{2, 5, 6}
	// // merge(arr1, 3, arr2, len(arr2))
	// // fmt.Println(arr1)
	// // root := &TreeNode{value: 1, right: &TreeNode{value: 2, left: &TreeNode{value: 3}}}
	// // inorderTraversal(root)
	// nums := []int{1, 2, 3}
	// perms := step1(nums)
	// for _, perm := range perms {
	// 	fmt.Println(perm)
	// }
	fmt.Println(findOddNumber(arr))
	fmt.Println(0^0, 0^3)

}

// findOddNumber
// 异或运算是一种逻辑运算，通常用符号“^”表示。它的运算规则是：两个相同位的数字相同则为0，不同则为1。例如，1^1=0，1^0=1，0^0=0。在计算机中，异或运算常用于数据加密、校验和计算、位图图像处理等方面
//
//	@Description: 可以使用异或运算来解决这个问题。异或运算的特点是，对于两个相同的数进行异或运算，结果为0；对于任何数和0进行异或运算，结果为该数本身。因此，我们可以将数组中的所有数进行异或运算，最终得到的结果就是出现奇数次的数
//	@param arr
//	@return int
func findOddNumber(arr []int) int {
	result := 0
	for _, num := range arr {
		result ^= num
	}
	return result
}

// 给定一个不含重复数字的整数数组 nums ，返回其 所有可能的全排列 。可以 按任意顺序 返回答案。
//
// 示例 1：
//
// 输入：nums = [1,2,3]
// 输出：[[1,2,3],[1,3,2],[2,1,3],[2,3,1],[3,1,2],[3,2,1]]
// 示例 2：
//
// 输入：nums = [0,1]
// 输出：[[0,1],[1,0]]
// 示例 3：
//
// 输入：nums = [1]
// 输出：[[1]]
// 使用回溯法实现全排列
func step2(arr []int, path []int, result *[][]int) {
	if len(path) == len(arr) {
		// 找到一个全排列，复制path到result
		tmp := make([]int, len(path))
		copy(tmp, path)
		*result = append(*result, tmp)
		return
	}

	for _, item := range arr {
		// 如果数字已经在path中出现过，则跳过
		if contains(path, item) {
			continue
		}
		// 将当前数字加入path
		path = append(path, item)
		// 递归调用
		step2(arr, path, result)
		// 回溯，将当前数字从path中移除
		path = path[:len(path)-1]
	}
}

// 辅助函数，判断一个整数是否在切片中
func contains(s []int, e int) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

// 主函数，返回所有全排列
func step1(nums []int) [][]int {
	var result [][]int
	step2(nums, []int{}, &result)
	return result
}

type TreeNode struct {
	value int
	left  *TreeNode
	right *TreeNode
}

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func inorderTraversal(root *TreeNode) (res []int) {
	if root == nil {
		return
	}
	res = inorderTraversal(root.left)
	res = append(res, root.value)
	res = append(res, inorderTraversal(root.right)...)
	return
}

// merge
//
//	@Description: 合并两递增数组
//
// 给你两个按 非递减顺序 排列的整数数组 nums1 和 nums2，另有两个整数 m 和 n ，分别表示 nums1 和 nums2 中的元素数目。
//
// 请你 合并 nums2 到 nums1 中，使合并后的数组同样按 非递减顺序 排列。
//
// 注意：最终，合并后数组不应由函数返回，而是存储在数组 nums1 中。为了应对这种情况，nums1 的初始长度为 m + n，其中前 m 个元素表示应合并的元素，后 n 个元素为 0 ，应忽略。nums2 的长度为 n 。
//
// 示例 1：
//
// 输入：nums1 = [1,2,3,0,0,0], m = 3, nums2 = [2,5,6], n = 3
// 输出：[1,2,2,3,5,6]
// 解释：需要合并 [1,2,3] 和 [2,5,6] 。
// 合并结果是 [1,2,2,3,5,6] ，其中斜体加粗标注的为 nums1 中的元素。
// 示例 2：
//
// 输入：nums1 = [1], m = 1, nums2 = [], n = 0
// 输出：[1]
// 解释：需要合并 [1] 和 [] 。
// 合并结果是 [1] 。
//
//	@param nums1
//	@param m
//	@param nums2
//	@param n
func merge(nums1 []int, m int, nums2 []int, n int) {
	for i, j, cur := m-1, n-1, m+n-1; i >= 0 || j >= 0; cur-- {
		if i == -1 {
			nums1[cur] = nums2[j]
			j--
		} else if j == -1 {
			nums1[cur] = nums1[i]
			i--
		} else if nums1[i] > nums2[j] {
			nums1[cur] = nums1[i]
			i--
		} else {
			nums1[cur] = nums2[j]
			j--
		}
	}
}

// reverseStr
//
//	@Description: 给定一个字符串 s 和一个整数 k，从字符串开头算起，每计数至 2k 个字符，就反转这 2k 字符中的前 k 个字符。
//
// 如果剩余字符少于 k 个，则将剩余字符全部反转。
// 如果剩余字符小于 2k 但大于或等于 k 个，则反转前 k 个字符，其余字符保持原样。
// 示例 1：
//
// 输入：s = "abcdefg", k = 2
// 输出："bacdfeg"
// 示例 2：
//
// 输入：s = "abcd", k = 2
// 输出："bacd"
//
//	@param s
//	@param k
//	@return string
func reverseStr(s string, k int) string {
	ss := []rune(s)
	for i := 0; i < len(ss); i = i + 2*k {
		sub := ss[i:int(math.Min(float64(i+k), float64(len(ss))))]
		for x, y := 0, len(sub)-1; x < y; x, y = x+1, y-1 {
			sub[x], sub[y] = sub[y], sub[x]
		}
	}
	return string(ss)
}

// reverse
//
//	@Description: 整数反转
//	@param x
//	@return int
func reverse(x int) int {
	tmp := 0
	for x != 0 {
		tmp = tmp*10 + x%10
		if tmp > math.MaxInt32 || tmp < math.MinInt32 {
			return 0
		}
		x = x / 10
	}
	return tmp
}

// climbStairs
// 每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？
//
//	@Description: 爬楼梯
//	@param n
//	@return int
func climbStairs(n int) int {
	// if n == 1 {
	// 	return 1
	// }
	// if n == 2 {
	// 	return 2
	// }
	// return climbStairs(n-1) + climbStairs(n-2)
	n1, n2, r := 0, 0, 1
	for i := 0; i < n; i++ {
		n1 = n2
		n2 = r
		r = n1 + n2
	}
	return r
}

// addBinary
//
//	@Description: 二进制求和,给你两个二进制字符串 a 和 b ，以二进制字符串的形式返回它们的和
//
// 输入:a = "11", b = "1"
// 输出："100"
//
//	@param a
//	@param b
//	@return string
func addBinary(a string, b string) string {
	result := ""
	carry := int64(0)
	n := len(a)
	if n < len(b) {
		n = len(b)
	}
	for i := 0; i < n; i++ {
		if i < len(a) {
			carry += int64(a[len(a)-1-i] - '0')
		}
		if i < len(b) {
			carry += int64(b[len(b)-1-i] - '0')
		}
		result = strconv.Itoa(int(carry%2)) + result
		carry /= 2
	}
	if carry > 0 {
		result = strconv.Itoa(int(carry)) + result
	}
	return result

}

// plusOne 加1
//
//	@Description: 给定一个由整数组成的非空数组所表示的非负整数，在该数的基础上加一。
//
// 最高位数字存放在数组的首位， 数组中每个元素只存储单个数字。
//
// 你可以假设除了整数 0 之外，这个整数不会以零开头。
//
//	@param digits
//	@return []int
func plusOne(digits []int) []int {
	p := 1
	for i := len(digits) - 1; i >= 0; i-- {
		digits[i] = digits[i] + p
		p = digits[i] / 10
		if p == 0 {
			return digits
		} else {
			digits[i] = digits[i] % 10
		}
	}
	if p > 0 {
		digits = append([]int{1}, digits...)
	}
	return digits
}

// lengthOfLastWord
// 给你一个字符串 s，由若干单词组成，单词前后用一些空格字符隔开。返回字符串中 最后一个 单词的长度。
//
// 单词 是指仅由字母组成、不包含任何空格字符的最大
// 子字符串
// 。
//
//	@Description: 最后一个单词的长度
//	@param s
//	@return int
func lengthOfLastWord(s string) int {
	if len(s) == 0 {
		return 0
	}
	start := false
	c := 0
	for i := len(s) - 1; i >= 0; i-- {
		if s[i] != ' ' {
			start = true
			c++
		} else if start {
			break
		}
	}
	return c
}

// searchInsert
//
//	@Description:搜索插入位置,
//	给定一个排序数组和一个目标值，在数组中找到目标值，并返回其索引。如果目标值不存在于数组中，返回它将会被按顺序插入的位置。
//
// 请必须使用时间复杂度为 O(log n) 的算法。
//
//	@param nums
//	@param target
//	@return int
func searchInsert(nums []int, target int) int {
	left, right := 0, len(nums)-1
	pos := len(nums)
	for left <= right {
		mid := (right + left) / 2
		if target <= nums[mid] {
			pos = mid
			right = mid - 1
		} else {
			left = mid + 1
		}
	}
	return pos
}

// strStr
//
//	@Description: 找出字符串中第一个匹配项的下标,
//	给你两个字符串 haystack 和 needle ，请你在 haystack 字符串中找出 needle 字符串的第一个匹配项的下标（下标从 0 开始）。如果 needle 不是 haystack 的一部分，则返回  -1
//	@param haystack
//	@param needle
//	@return int
func strStr(haystack string, needle string) int {
	for i := 0; i < len(haystack); i++ {
		if isSubStr(haystack[i:], needle) {
			return i
		}
	}
	return -1
}
func isSubStr(s1, s2 string) bool {
	if len(s2) > len(s1) {
		return false
	}
	for i := 0; i < len(s2); i++ {
		if s1[i] != s2[i] {
			return false
		}
	}
	return true
}

// removeElement
//
//	@Description: 给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。
//
// 不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并 原地 修改输入数组。
//
// 元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。
//
//	@param nums
//	@param val
//	@return int
func removeElement(nums []int, val int) int {
	if len(nums) > 0 {
		for i := 0; i < len(nums); i++ {
			if nums[i] == val {
				nums = append(nums[:i], nums[i+1:]...)
				i--
			}
		}
		nums = append([]int{}, nums...)
	}
	return len(nums)
}

// removeDuplicates
//
//	@Description: 移除有序数组中重复的元素，
//
// [0 0 1 1 1 2 2 3 3 4]->[0 1 2 3 4]
//
//	@param nums
//	@return int
func removeDuplicates(nums []int) int {
	if len(nums) > 1 {
		for i := len(nums) - 1; i > 0; i-- {
			if nums[i] == nums[i-1] {
				nums = append(nums[:i], nums[i+1:]...)
			}
		}
	}
	return len(nums)
}

type ListNode struct {
	Val  int
	Next *ListNode
}

// mergeTwoLists
//
//	@Description: 合并两个有序链表
//	@param list1
//	@param list2
//	@return *ListNode
func mergeTwoLists(list1 *ListNode, list2 *ListNode) *ListNode {
	r := &ListNode{}
	root := r
	for list1 != nil && list2 != nil {
		if list1.Val < list2.Val {
			r.Next = list1
			list1 = list1.Next
		} else {
			r.Next = list2
			list2 = list2.Next
		}
		r = r.Next
	}
	if list1 != nil {
		r.Next = list1
	}
	if list2 != nil {
		r.Next = list2
	}
	return root.Next
}

// isValidCom
//
//	@Description: 判断是否是有效括号{}()[]成对出现
//	@param com
//	@return bool
func isValidCom(com string) bool {
	if len(com)%2 != 0 {
		return false
	}
	m := map[int32]int32{
		')': '(',
		']': '[',
		'}': '{',
	}
	var stack []int32
	for _, c := range com {
		if r, ok := m[c]; ok && len(stack) != 0 && stack[len(stack)-1] == r {
			stack = stack[:len(stack)-1]
		} else {
			stack = append(stack, c)
		}
	}
	return len(stack) == 0
}

// longestCommonPrefix
//
//	@Description: 查找最长公共子序列
//	@param strs
//	@return string
func longestCommonPrefix(strs []string) string {
	if len(strs) == 0 {
		return ""
	}
	prefixStr := strs[0]
	for i := 1; i < len(strs); i++ {
		prefixStr = findCommonPrefix(prefixStr, strs[i])
	}
	return prefixStr
}

func findCommonPrefix(str1, str2 string) string {
	idx := 0
	l := len(str1)
	if len(str1) > len(str2) {
		l = len(str2)
	}
	for i := 0; i < l; i++ {
		if str1[i] == str2[i] {
			idx++
		} else {
			break
		}
	}
	return str1[:idx]
}

// isPalindrome
//
//	@Description: 判断是否是回文数字，如123321是，12345不是
//	@param x
func isPalindrome(x int) {
	s := fmt.Sprintf("%d", x)
	ss := []rune(s)
	for i, j := 0, len(ss)-1; i < j; i, j = i+1, j-1 {
		if ss[i] != ss[j] {
			fmt.Println(false)
			return
		}
	}
	fmt.Println(true)
}

// wordCount
//
//	@Description: 计算字符串中单词个数，以空格分隔
//	@param s
//	@return n
func wordCount(s string) {
	if len(s) == 0 {
		fmt.Println(0)
	}
	n := 0
	for idx, c := range s {
		if (idx == 0 || s[idx-1] == ' ') && c != ' ' {
			n++
		}
	}
	fmt.Println(n)
}

// twoSum
//
//	@Description: 两数之和，在数组中寻找两数之和为给定的数
//	@param arr
//	@param target
//	@return []int
func twoSum(arr []int, target int) {
	m := map[int]struct{}{}
	for _, item := range arr {
		d := target - item
		if _, ok := m[d]; ok {
			fmt.Println([]int{d, item})
		} else {
			m[item] = struct{}{}
		}
	}
}
