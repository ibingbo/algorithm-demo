package src

import (
	"fmt"
	"testing"
)

func TestSortAlgorithm_Quicksort(t *testing.T) {
	arr := []int{3, 2, 4, 6, 1, 5}
	fmt.Println(arr)
	DefaultSortAlgorithm.Quicksort(arr, 0, len(arr)-1)
	fmt.Println(arr)
}
