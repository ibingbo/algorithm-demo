package src

var DefaultSortAlgorithm = &SortAlgorithm{}

type SortAlgorithm struct {
}

func (a *SortAlgorithm) Quicksort(arr []int, start int, end int) {
	if start >= end {
		return
	}
	key := arr[start]
	i, j := start, end
	for i < j {
		for i < j && arr[j] >= key {
			j--
		}
		arr[i] = arr[j]
		for i < j && arr[i] <= key {
			i++
		}
		arr[j] = arr[i]
	}
	arr[i] = key
	a.Quicksort(arr, start, i-1)
	a.Quicksort(arr, i+1, end)
}
